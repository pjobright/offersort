chrome.runtime.onMessage.addListener(function(request){

    /*

    Code to hide facebook like buttons. Doesn't belong here, just for testing.

    */

    document.querySelectorAll('li').forEach((item) => {
        if(item.innerText == 'Like') {
            // item.remove();
            item.style.display = "none";
        }
    });

    document.querySelectorAll('i').forEach((item) => {
        if(item.parentNode.parentNode.innerText == 'Like') {
            // item.parentNode.parentNode.remove();
            item.parentNode.parentNode.style.display = "none";
        }
    });

    // TODO make this snippet work in the background

    // END


    if(request == "sortOffers") {
        var prodList = document.querySelectorAll("ul.fops-regular li.fops-item")
        var offersList = [];
        for(const prod of prodList) {
            if(prod.classList.contains("fops-item--advert")) {
                prod.style.display = "none"
            }
            else if(prod.querySelector(".oos-info")) {
                prod.style.display = "none"
            }
            else if(!prod.querySelector(".fop-old-price")) {
                prod.style.display = "none"
            }
            else {
                // let oldprice = prod.querySelector(".fop-old-price").textContent.substring(1);
                // let newprice = prod.querySelector(".fop-price").textContent.substring(1);
                let oldprice = prod.querySelector(".fop-old-price").textContent.replace('£','').replace('p','');
                let newprice = prod.querySelector(".fop-price").textContent.replace('£','').replace('p','');
                let discount = Math.round((oldprice - newprice) / oldprice * 100);
                prod.querySelector("h4").innerHTML =  '<b>' + discount + '% off</b> ' + prod.querySelector("h4").innerHTML
                offersList.push({
                    "discount" : discount,
                    "prod" : prod
                })
            }
        }

    
        offersList.sort((a,b) => (a.discount < b.discount) ? 1 : -1)

        // var sortedList = [];

        for(const curr of offersList) {
            // sortedList.push(curr.prod)
            document.querySelector("ul.fops-regular").appendChild(curr.prod);
        }





    }
    else if(request == "sortNumRatings") {

        if(window.location.origin.includes("ocado.com")) {


            var prodList = document.querySelectorAll("ul.fops-regular li.fops-item")

            var prodSortList = []

            for(const prod of prodList) {
               var ratingCount = prod.querySelector("span.fop-rating__count")
                   
               if(ratingCount) {
                   prodSortList.push({
                       "ratingCount" : ratingCount.textContent.replace('(','').replace(')','') + 0,
                       "prod": prod
                   })

               }             
               else {
                   prod.style.display = "none"
               }
            }

            prodSortList.sort((a,b) => (a.ratingCount - b.ratingCount < 0) ? 1 : -1)
        
            for(const curr of prodSortList) {
                document.querySelector("ul.fops-regular").appendChild(curr.prod);
            }
        }
        else if(window.location.origin.includes("amazon")) {

            let filteredProdList = sortAmazonProductsByNumberOfReviews(getAmazonProductsListWithReviews());


            let savedProdList = getOrClearSavedAmazonProucts(window.location.href);
            filteredProdList.forEach((currProd) => document.querySelector("div.s-search-results").insertBefore(currProd.prod, document.querySelector("div.s-search-results").firstChild));
            saveAmazonProductsListToStorage(filteredProdList);

        }

    }

    else if(request == "sortPrice") {

            let filteredProdList = sortAmazonProductsByPrice(getAmazonProductsListWithReviews());
            // filteredProdList = [..., getAmazonProductsListFromStorage()];
            getAmazonProductsListFromStorageAndSortBy("sortPrice", filteredProdList);

    }

    else if(request == "sortPricePer") {
        if(window.location.origin.includes("trolley.co.uk")) {

            var prodList = document.querySelectorAll("div.products-grid div.product-item");

            var prodSortList = []

            for(const prod of prodList) {
                var pricePerDiv = prod.querySelector("div._per-item")
                console.log(prod)
                if(pricePerDiv) {
                    var pricePer = pricePerDiv.innerText.replace('£','').replace(' per 100ml','')
                    console.log(pricePerDiv.innerText)
                    console.log(pricePer)
                    prodSortList.push({
                        "pricePer" : pricePer,
                        "prod" : prod
                    })
                }
                else {
                    prod.style.display = "none"
                }

            }
            prodSortList.sort((a,b) => (a.pricePer - b.pricePer > 0) ? 1 : -1)
            console.log(prodSortList)

            for(const curr of prodSortList) {
                document.querySelector("div.products-grid").appendChild(curr.prod)
            }

        }
    }



})


function getOrClearSavedAmazonProucts(urlString) {
}

function isSameAmazonSearch(oldurl, newurl) {
    return trimAmazonURL(newurl) == oldurl;
}

function trimAmazonURL(urlString) {
    return urlString.replace(/[&]ref=sr_pg_[0-9]{1,2}/,'').replace(/[&]qid=[0-9]{9,11}/,'').replace(/[&]page=[0-9]{1,2}/,'');
}

// function removeAllChildNodes(parent) {
//     while (parent.firstChild) {
//         parent.removeChild(parent.firstChild);
//     }
// }

function sortAmazonProductsByNumberOfReviews(prodsList) {
    return prodsList.sort((a,b) => (a.ratingCount - b.ratingCount < 0) ? -1 : 1);
}

function sortAmazonProductsByPrice(prodsList) {
    return prodsList.sort((a,b) => (a.price - b.price > 0) ? -1 : 1);
}

function saveAmazonProductsListToStorage(prodsList) {

    prodsListHTML = prodsList.map((item) => {return  {...item, prod : item.prod.outerHTML}});

    console.log(prodsListHTML);

    chrome.storage.local.set({
        "amazonProductsList" : prodsListHTML
    });
}


// async function getData() {
//     let data = await getAmazonProductsListFromStorage();
//     console.log('data');
//     console.log(data);
//     return data;
// }


// function getAmazonProductsListFromStorage() {

//     let p = new Promise((resolve, reject) => {
//         chrome.storage.sync.get(["amazonProductsList"], function(items) {
//             console.log(items);
//             resolve(items);
//         });
//     });

//     p.then((items) => {
//         console.log(items);
//         return items;
//     });


// }


function getAmazonProductsListFromStorageAndSortBy(sortOrder, prodsList) {
    chrome.storage.local.get(["amazonProductsList"], function(items){
        prodsList = [... prodsList, ... items.amazonProductsList];

        let sortedProdsList = [];

        if(sortOrder == 'sortPrice') {
            sortedProdsList = sortAmazonProductsByPrice(prodsList);
        }
        else {
            sortedProdsList = sortAmazonProductsByNumberOfReviews(prodsList);
        }

        sortedProdsList.forEach((currProd) => {
            // console.log(currProd.prod);
            // console.log(document.createRange().createContextualFragment(currProd.prod));
            if(typeof(currProd.prod) == 'string') {
                document.querySelector("div.s-search-results").insertBefore(document.createRange().createContextualFragment(currProd.prod), document.querySelector("div.s-search-results").firstChild);
            }
            else {
                document.querySelector("div.s-search-results").insertBefore(currProd.prod, document.querySelector("div.s-search-results").firstChild);
            }
                // console.log('\n\n\n\n\n\n\n Prod : ');
                // console.log(currProd.prod);
                // console.log('Converted Node :');
                // console.log(document.createRange().createContextualFragment(currProd.prod));
        });
    });
}



function getAmazonProductsListWithReviews() {

    let filteredProdList = [];

    // Get all product search results
    document.querySelectorAll('div[data-component-type="s-search-result"]').forEach((item, index) => {


        let hasReviews = false;
        let hasPrice = false;
        let numReviews = 0;
        // Iterate through all spans                
        item.querySelectorAll('span').forEach((spanItem, index) => {
            let ariaLabel = spanItem.getAttribute("aria-label");

            // Identify the span with star rating
            if(ariaLabel != undefined && ariaLabel.includes(" ratings")) {

                hasReviews = true;
                // numReviews = spanItem.nextSibling.getAttribute("aria-label");
                numReviews = spanItem.getAttribute("aria-label").replace(" ratings","");
                console.log(numReviews);

            }

        });

        let priceWhole = item.querySelector("span.a-price-whole");
        let priceFraction = item.querySelector("span.a-price-fraction");


        let priceWholeNumber = null;

        if(priceWhole != null) {
            hasPrice = true;
            priceWholeNumber = priceWhole != null ? priceWhole.innerText.match(/(\d+)/) : 0;
        }

        if(hasReviews) {
            filteredProdList.push({
                "ratingCount" : Number(numReviews.replace(',','')),
                "price" : hasPrice ? Number(priceWholeNumber[0]) : 99999,
                "prod" : item
            });
        }
        else {
            item.parentNode.removeChild(item);
        }
                // removed for india as there are no paise
                // "price" : hasPrice ? Number(priceWholeNumber[0]) + Number(priceFraction.innerText / 100) : 99999,

    });

    return filteredProdList;

}

