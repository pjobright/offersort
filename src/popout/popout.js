document.addEventListener('DOMContentLoaded', function(){

    document.querySelector('button.sortOffers').addEventListener('click', onclickSortOffers, false)

    function onclickSortOffers () {

        chrome.tabs.query({currentWindow: true, active: true},
            function (tabs) {

                chrome.tabs.sendMessage(tabs[0].id, 'sortOffers')                
            
            }
        )

    }

    document.querySelector('button.sortNumRatings').addEventListener('click', onclicksortNumRatings, false)

    function onclicksortNumRatings () {

        chrome.tabs.query({currentWindow: true, active: true},
            function (tabs) {

                chrome.tabs.sendMessage(tabs[0].id, 'sortNumRatings')                
            
            }
        )

    }



    document.querySelector('button.sortPrice').addEventListener('click', onclicksortPrice, false)

    function onclicksortPrice () {

        chrome.tabs.query({currentWindow: true, active: true},
            function (tabs) {

                chrome.tabs.sendMessage(tabs[0].id, 'sortPrice')                
            
            }
        )

    }

    document.querySelector('button.sortPricePer').addEventListener('click', onclicksortPricePer, false)

    function onclicksortPricePer () {

        chrome.tabs.query({currentWindow: true, active: true},
            function (tabs) {

                chrome.tabs.sendMessage(tabs[0].id, 'sortPricePer')                
            
            }
        )

    }


}, false)